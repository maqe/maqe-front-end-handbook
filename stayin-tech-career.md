# Developer Carrer path
## Technical career path (No Managerial stuffs)
This is example and some references, if you would like to stay in Technical career path rather than Manager. **Watch this first
[Rethinking the Developer Career Path](https://www.youtube.com/watch?v=yIPbE7BssOs)**

### Role & Title in Technical path:
	
- **Specialist**: You create libraries: Redux, Slick, Babel, Big.js
- **Advisor**: Someone admired you and asking you for advise in your own area.
- **Developer Advocate**: Become a dev who pushing tech forward/become well-known by giving example/experiment/speaker
	- [Developer Advocates at Google](https://www.youtube.com/watch?v=MFLZB8Q8lT8)
	- [Devloper Advocates at Microsoft](https://developer.microsoft.com/en-us/advocates/)
- **Teacher/Trainer**: You're comfortable to create training course and be a tutor
- **Architect**: You can design and help create reliable/scale/robust software system.
- **Influencer** in IT Industry

Specialist role: Network, Database, Security, DevOps

### Role models:
Below are people who write code for living but also contribute and create impact to IT community/industry.

- Advocate, Evangelist: Sr. Developer Advocate @Microsoft. @vuejs Core Team. [Sarah Drasner](https://twitter.com/sarah_edo)
- [Redux creator](https://medium.com/@dan_abramov)
- [Slick creator](http://kenwheeler.github.io/)
- [Babel creator](https://twitter.com/sebmck?lang=en)
- [Teacher MMPJ (ex-spotify dev)](https://www.youtube.com/watch?v=568g8hxJJp4)

### Where am I after 30 years in IT industry?
* Start with FrontEnd
* WebDeveloper: Know backend so can create apps.
* Full stack: Know FE, BE, DevOps, Database, Performance; all in every aspects
* Finally, I would like to try eveything new: MicroController, VR, Mobile App, IoT, Game

### Food for thought
At some point there is **new generation** that can write code and deliver same as you with cheaper price.

The difference are **'Experience'** and **'Failure'** that you learned in the past.

> So How can you stay in tech industry? You have to keep update and learn in other areas. **The better you know how everything works the better you will be at creating specialized stuff.**

> A developer with many years of experience, can see the bigger picture and knows what�s needed to be done, whereas a newbie will get hung up on details and lose sight of his objective.

> A professional will give you what you need, an amateur will give you what you want.

> A good developer knows how to balance his time between the tasks at hand, while an amateur has no idea on where to start.

### Examples
You have very strong JS but you try to

- Create mobile app but then you may need to learn about Firebase
- [Create micro controller](https://www.javascriptjanuary.com/blog/rapid-prototyping-javascript-and-hardware) but you may need to learn how to connect hardward board
- Create VR but you need to learn how CG works