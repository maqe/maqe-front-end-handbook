# Learning Front-end skills

This should not be considered a comprehensive outline of all resources available to a front-end developer. The value of the book is tied up in a terse, focused, and timely curation of just enough categorical information so as not to overwhelm anyone on any one particular subject matter.

## Web

- [How does webservers work](https://youtu.be/eqlZD21DME0) it using Node.js to demonstrate serving HTTP content. If you alredy knew Node.js you can skip to 10:00 minutes
- [What is DNS?](https://howdns.works/) Nice comics! about DNS

## Html & CSS
- [Intro to HTML/CSS](https://www.khanacademy.org/computing/computer-programming/html-css) [Watch / Interact]
- [Learn to Code HTML & CSS](http://learn.shayhowe.com/html-css/)
- [HTML Glossary](https://www.codecademy.com/articles/glossary-html)
- [Learn CSS Layout](http://learnlayout.com/)

## Javascripts & DOM
- [JS and the DOM](https://www.khanacademy.org/computing/computer-programming/html-css-js#js-and-the-dom) [Interact] There are many topics under DOMs please go through it all.
- [You don't know JS](https://github.com/getify/You-Dont-Know-JS) This one is a good introduction to JS reading, if you want to better know JS. Read online free.
	* [Up&Going](https://github.com/getify/You-Dont-Know-JS/blob/master/up%20&%20going/README.md#you-dont-know-js-up--going)
	* [Types & Grammar](https://github.com/getify/You-Dont-Know-JS/blob/master/types%20&%20grammar/README.md#you-dont-know-js-types--grammar)
	* [Scope & Closures](https://github.com/getify/You-Dont-Know-JS/blob/master/scope%20&%20closures/README.md#you-dont-know-js-scope--closures)

## JSON
- [What is JSON?](https://mijingo.com/lessons/what-is-json/)

## JS Template: Handlebar
- [Handlebars JavaScript Templates in 8 minutes](https://www.youtube.com/watch?v=3zVYH16yogQ)

---
### After you finish all of above topics you are ready!