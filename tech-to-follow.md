# Technology list to follow

## Newsletter
- http://csslayout.news/
- https://sidebar.io/
- https://dev.to/
- https://www.webdesignernews.com/
- https://uianimationnewsletter.com/
- http://frontendweekly.co/

## Medium
- https://codeburst.io/
- https://uxdesign.cc/
- https://medium.freecodecamp.org/
- https://ux.shopify.com/
- https://blog.knowyourcompany.com/

## Twitter account
- vpieters
- litmusapp
- sarah_edo
- CodePen
- DesignerDepot
- scotch_io
- Nick_Craver
- eli_schiff
- ChrisFerdinandi
- UXBooth
- css
- getify
- frontstuff_io
- smashingmag
- dan_abramov
- jensimmons
- JavaScriptKicks
- JavaScriptDaily

## Youtube
- [JS Conf](https://www.youtube.com/user/jsconfeu)
- [NDC Conferences](https://www.youtube.com/channel/UCTdw38Cw6jcm0atBPA39a0Q)
- [OreillyMedia](https://www.youtube.com/user/OreillyMedia)
- [Learn Code Academy](https://www.youtube.com/user/learncodeacademy)
- [Chrome Developers](https://www.youtube.com/user/ChromeDevelopers)
- [Coding Tech](https://www.youtube.com/channel/UCtxCXg-UvSnTKPOzLH4wJaQ)
- [Siraj Raval - Data sci](https://www.youtube.com/channel/UCWN3xxRkmTPmbKwht9FuE5A)
- [freeCodeCamp.org](https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ)
- [GOTO Conferences](https://www.youtube.com/channel/UCs_tLP3AiwYKwdUHpltJPuA)

## Forums
- [Reddit - Programming](https://www.reddit.com/r/programming/)
- [Reddit - Javascript](https://www.reddit.com/r/javascript/)
- [Hacker news](https://news.ycombinator.com/)