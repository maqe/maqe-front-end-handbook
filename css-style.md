   
# HTML/CSS styleguide

This is similar to [**BEM**](http://getbem.com/introduction/) system but not as strict or structured.
We adopt good things from **BEM** and customize some to use it our own way :)  

## **HTML**

### Character encoding
Quickly and easily ensure proper rendering of your content by declaring an explicit character encoding. When doing so, you may avoid using character entities in your HTML, provided their encoding matches that of the document (generally UTF-8)
```html
<head>
  <meta charset="UTF-8">
</head>
```

### No need to specify type when CSS and JavaScript includes
```html
<!-- External CSS -->
<link rel="stylesheet" href="code-guide.css">

<!-- In-document CSS -->
<style>
  /* ... */
</style>

<!-- JavaScript -->
<script src="code-guide.js"></script>
```

### Naming in lowercase
```html
class="section-header"
```

### Put no space if name has spaces
If the name of the element has space(s) between the words, name the class without spaces.
```html
class="likebutton"
```

### Start with component name
- The first name is often the name of the widget, component or feature which you are styling E.g. article, slideshow, contact, search, etc.
- Subsequent names are often parts of portions of the component E.g. title, button, image, etc.
- Separate parent-child relationship by a dash.

```html
<header class="header">
    <img class="header-logo" src="http://placehold.it/200x150" />
    <h2 class="header-title">MAQE</h2>
</header>
```

### If name is too long
because it is deeply nested, keep the first parent’s element name, remove some following element names as necessary, and keep the last two or three element names.

#### Bad
```html
<footer class="footer">
    <div class="footer-socialmedia">
        <a><img class="footer-socialmedia-facebook-icon" src="http://placehold.it/50x50" />Facebook</a>
    </div>
</footer>
```

#### Good
```html
<footer class="footer">
    <div class="footer-socialmedia">
        <a><img class="footer-facebook-icon" src="http://placehold.it/50x50" />Facebook</a>
    </div>
</footer>
```

### Wrapper
Wrappers are a special case. Use the name of the element followed with a dash and text “wrapper”. In this case, it does not mean they have parent-child relationship.

### Avoid binding to the same class in both your CSS and JavaScript
We recommend creating JavaScript-specific classes to bind to, prefixed with **.js-share-fb**, or even if it is for selenium test it should uses **data-attribute**
```html
<div class="footer-socialmedia js-socialmedia-control"></div>
<div class="footer-socialmedia" data-selenium="footer-socialmedia"></div>
```

## **SASS, LESS**

### Common rules
- Do not use ID selectors
    - ID selectors have priority over other kinds of selectors. This can make it harder to add new rules using less specific selectors.
    - This doesn't mean you shouldn't use them but does mean that you should be very specific with your usage of them.
- Put a space before the opening brace { in rule declarations
In properties, put a space after, but not before, the : character.
- When using multiple selectors in a rule declaration, give each selector its own line.
- Put closing braces } of rule declarations on a new line
- Put blank lines between rule declarations
- Use **REM** units or something scale, unless no choice then **PX**
- PX cases: border, position that may less than 5 px
- Avoid @extend
    * [Avoid sass extend](https://www.sitepoint.com/avoid-sass-extend/)
    * [Sass without mess](https://www.smashingmagazine.com/2015/05/extending-in-sass-without-mess/)


### Use ID selector carefully
```scss
#pizza {
 color: blue;
}

#pizza.red {
 color: red;
}

<div id="pizza" class="red"></div>
```

You can't just define red and use it. You need to first override the pizza id in terms of specificity and then redefine the color. [Quora answer](https://www.quora.com/Why-is-it-considered-bad-form-to-use-an-ID-selector-in-a-CSS-stylesheet)

### New line when it has multiple selectors
**Bad**

```css
.avatar{
    border-radius:50%;
    border:2px solid white; }
.no, .nope, .not_good {
    // ...
}
#lol-no {
  // ...
}
```

**Good**

```css
.avatar {
  border-radius: 50%;
  border: 2px solid white;
}

.one,
.selector,
.per-line {
  // ...
}
```

### Variable name
- Use lowercase and hyphen
- Name with meaning

**Bad**
```css
@color-1: #49c770;
@color-2: #fefefe;
```
**Good**
```css
@color-primary: #49c770;
@color-button-hover: #fefefe;
```

### Component based
Use first parent as a first-level block, and nest all children inside the block. Highly specified children selectors can be placed standalone but we often nest to help with legibility, structure and code re-use.

```scss
.header {
    color: #000;

    .header-logo {
        width: 200px;
    }

    .header-title {
        color: #000;
    }

    .header-title:hover {
        color: #000;
    }
}
```

### Nested selectors
**Do not nest selectors more than three levels deep!** **Why?**: because it is hard to override this style if you have to.

When selectors become this long, you're likely writing CSS that is:

* Strongly coupled to the HTML (fragile) *—OR—*
* Overly specific (powerful) *—OR—*
* Not reusable

**Bad**
```scss
.page-container {
  .content {
    .profile {
      // STOP!
    }
  }
}
```

**Good**

Try to name it as one level selector
```scss
.page-container {
  
}
.page-content {
    
}
.page-profile {
    
}
```

### Use Classname instead of Element selector
Elements could be changed depends on context but classname usually stay the same

**Bad**

It is UL list for now.
```html
<ul class="person-list">
    <li>This is person item</li>
    <li>This is person item</li>
</ul>
```
```scss
.person-list {
    margin: 0;

    li {
        font-size: 2rem;
    }
}
```

**Good**

But then it needs to change into DIV blocks for some reasons, In this way we no need to change css if we use class selector at a beginning.
```html
<div class="person-list">
    <div class="person-item">This is person item</div>
    <div class="person-item">This is person item</div>
</div>
```
```scss
.person-list {
    margin: 0;

    .person-item {
        font-size: 2rem;
    }
}
```

### Insert a status class next to element classes.
```scss
.article {
    .article-content {
        display: none;
        color: #000;
    }

    .article-content.isactive {
        display: block;
    }
}
```

### Ordering of property declarations
Related property declarations should be grouped together following the order:

1. Positioning
2. Box model
3. Typographic
4. Visual
```scss
.declaration-order {
    /* Positioning */
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 100;

    /* Box-model */
    display: block;
    float: right;
    width: 100px;
    height: 100px;

    /* Typography */
    font: normal 13px "Helvetica Neue", sans-serif;
    line-height: 1.5;
    color: #333;
    text-align: center;

    /* Visual */
    background-color: #f5f5f5;
    border: 1px solid #e5e5e5;
    border-radius: 3px;

    /* Misc */
    opacity: 1;
}
```

#### Property declarations

List all standard property declarations, anything that isn't an `@include` or a nested selector.

```scss
.btn-green {
    background: green;
    font-weight: bold;
    // ...
}
```



#### `@include` declarations

Grouping `@include`s at the end makes it easier to read the entire selector.

```scss
.btn-green {
    background: green;
    font-weight: bold;
    @include transition(background 0.5s ease);
    // ...
}
```

#### Nested selectors

Nested selectors, _if necessary_, go last, and nothing goes after them. Add whitespace between your rule declarations and nested selectors, as well as between adjacent nested selectors. Apply the same guidelines as above to your nested selectors.

```scss
.btn {
    background: green;
    font-weight: bold;
    @include transition(background 0.5s ease);

    .icon {
        margin-right: 10px;
    }
}
```

