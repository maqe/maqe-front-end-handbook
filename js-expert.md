This all maybe just starter topics; but heop that this can help you figure out what you can **google** and read more about it.  Try to understand **foundation** and why it exist for what purpose. Feel free to take a deep dive on topic you like.

## JS Core
- [this, object, prototype, class](https://github.com/getify/You-Dont-Know-JS/tree/master/this%20%26%20object%20prototypes)
    - So after read it what do you prefer?
- [Many ways to create javascript class](https://addyosmani.com/resources/essentialjsdesignpatterns/book/#categoriesofdesignpatterns)
- [Classic JS Extend](https://stackoverflow.com/questions/3781373/javascript-what-are-extend-and-prototype-used-for)
    - Why we need that?
- [Mixin](https://javascript.info/mixins)
    - Why we need that?

## Basic of JS frameworks
- 1 way binding
- 2 ways binding
- Pub/Sub pattern
    - [How it works?](https://www.toptal.com/ruby-on-rails/the-publish-subscribe-pattern-on-rails)
    - [Another example](https://msdn.microsoft.com/en-us/magazine/hh201955.aspx?f=255&MSPPError=-2147217396)
    - Try, to write your own one simple app
- Observer pattern
    - [How it works?](https://hackernoon.com/observer-vs-pub-sub-pattern-50d3b27f838c)
    - Try, to write your own one simple app

## Real usage in JS programming world
- There are many pattern? So what should i use? Find an answer for it
- [EventEmitter](https://www.datchley.name/es6-eventemitter/)
- Redux
    - Find out why we use it?
    - Try to create todo app WITHOUT Redux but use EventEmitter

## Good to know
- [Right your own js browser routing](http://blog.teamtreehouse.com/getting-started-with-the-history-api)
- What is Immutable
    - Why we need it?
- What is package manager?
    - What is cURL?
    - [How NPM works?](https://www.youtube.com/watch?v=zWEU8kNKi3Q)
- What is �[First meaningful paint](https://www.youtube.com/watch?v=4pQ2byAoIX0)� ?

## Before you start React
- [Virtual Dom](https://medium.com/@deathmood/how-to-write-your-own-virtual-dom-ee74acc13060)
- [Wanna write your own?](http://reactkungfu.com/2015/10/the-difference-between-virtual-dom-and-dom/)
- I think at this point you already know why React can be used to write Mobile app, right?
