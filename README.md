# Front-End Engineer handbook #

This is a guide that anyone could use to learn about the practice of front-end development. It broadly outlines and discusses the practice of front-end engineering: how to learn it and what tools are used when practicing it.

## Development guidelines and good practices
- [CSS styleguide](css-style.md)
- [JS styleguide](https://bitbucket.org/maqe/maqe-javascript-style-guide)

---

## Tech stack

This section describes which tools we use and do not use at MAQE. We categorize them into 4 categories.

- **Adopt:** We use these tools regularly in our work.
- **Evaluating:** We have some interest in these tools and try them in POCs, side projects, etc. Some projects may use these tools but we are not confident to say that it's our main stack yet.
- **Transitioning out:** We used these tools in our works in the past, but we have stopped using them in new works. Only use them in legacy works.
- **Avoid:** Tools in this category are either too old, not good enough, or we have no interest in investing in them anytime soon.

Last update in 2022.

### Languages
- Adopt
	- HTML
	- CSS
	- SCSS
	- JavaScript
	- TypeScript
- Evaluating
- Transitioning out
	- LESS
- Avoid
	- SASS
	- Stylus
	- CoffeeScript

### Frameworks & Template engines
- Adopt
	- Vue
	- Nuxt
	- React
	- Next
	- WordPress
- Evaluating
	- Svelte
	- SolidJS
	- EJS
	- Tailwind CSS
- Transitioning out
	- jQuery
	- Bootstrap
- Avoid
	- Angular
	- Ember
	- Pug

### Testing tools
- Adopt
	- Jest
	- Cypress
- Evaluating
	- Vitest
- Transitioning out
- Avoid
	- wd.js
	- WebdriverIO

### Build tools
- Adopt
	- Webpack
- Evaluating
	- Vite
	- Rollup
	- Parcel
- Transitioning out
	- Gulp
	- Browserify
- Avoid
	- Grunt

---

### Find out what level are you

- [HTML test](https://www.w3schools.com/quiztest/quiztest.asp?qtest=HTML)
- [CSS test](https://www.w3schools.com/quiztest/quiztest.asp?qtest=CSS)
- [JS test](https://www.w3schools.com/quiztest/quiztest.asp?qtest=JavaScript)

If you got a score under 50% (on average) then you're a beginner Front-End Engineer and you should start with

### **[Learning basic Front-End skills](front-end-skills.md)**.

---
This is a **[Front-End on-site test](https://docs.google.com/document/d/1ujfrw6DOkCiscSZTk_P-SykQYDfCKNRzYAeADTEmMj8)**, if you can do it then **you are in advance mode!**

After you learn basic Front-End skills, it would be better if we have the same code style when we're working together so we can write code in the same way, easy to read and maintain.

## Front-End skills in-depth
If you would like to know what going on in WEB DEVELOPMENT in 2016/2017 [watch this](https://www.youtube.com/watch?v=sBzRwzY7G-k)

- **[Technology list to follow](tech-to-follow.md)**
- UI/UX Interaction
	- [Deep dive CSS: font metrics, line-height and vertical-align](http://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align)
	- [Type of web animation](https://www.awwwards.com/web-animation-infographics-a-map-of-the-best-animation-libraries-for-javascript-and-css3-plus-performance-tips.html)
	- [Web Animation — A comprehensive overview](https://medium.com/@evejweinberg/web-animation-everything-you-need-to-know-in-too-much-detail-91bf5d48f980)
	- This is how the designer works with animation
		- [All you need to know about animation](https://www.kirupa.com/html5/learn_animation.htm)
		- Learn how animation works first with Flash (I know it’s old but it’s very useful): https://www.youtube.com/watch?v=wrqpcnz-rTM
	- Then how can we control it by programming
		- Create geometry in CSS: [watch this](https://www.youtube.com/watch?v=ytroKBCdkZg)
		- Limitation is your imagination:
			- [Make CSS Your Secret Super Drawing Tool](https://www.youtube.com/watch?v=mNKz3devFAw)
			- [Animating circles with fake shadows](https://www.youtube.com/watch?v=gP6Ij3IhudE)
		- Greensock start: [watch this](https://www.youtube.com/watch?v=tMP1PCErrmE)
			- [SVG and GreenSock for Complex Animation](https://www.youtube.com/watch?v=ZNukcHhpSXg)
			- [Learn to Create an SVG Knockout Mask Animation](https://www.youtube.com/watch?v=zwRLMDBGTAk)

		- WebGL:
			- Next level with 3D Three.js: [watch this](https://www.youtube.com/watch?v=ZBGX9C74WFE)
			- More Three.js: [watch this](https://www.youtube.com/watch?v=biZgx45Mzqo)
			- [Practical WebGL with Three.js](https://www.youtube.com/watch?v=wRXLs-Ccj2o)

- Email template
	- [Ideas Behind Responsive Emails](https://css-tricks.com/ideas-behind-responsive-emails/)
	- [Simple email responsive](https://webdesign.tutsplus.com/articles/creating-a-simple-responsive-html-email--webdesign-12978)
	- [Watch it with 2X speed](https://www.youtube.com/watch?v=7FcVjXkNH4g&t=911s)
	- CSS3 may not available to use, so stick with old ways if need to support [link](https://www.campaignmonitor.com/blog/email-marketing/2010/04/css3-support-in-email-clients/)
	- Tools:
		- Check CSS compatibility on specific email client [link](https://www.campaignmonitor.com/css/)
		- Blog about emails [link](https://www.emailonacid.com/blog/)

- Programming
	- [Design Patterns for Humans](https://github.com/kamranahmedse/design-patterns-for-humans)
	- [Modern JavaScript Explained For Dinosaurs](https://medium.com/@peterxjang/modern-javascript-explained-for-dinosaurs-f695e9747b70)
	- [The Art of Comments](https://css-tricks.com/the-art-of-comments/)

- Testing
	- [Karma page is a very good starting point](https://karma-runner.github.io/1.0/index.html)
	- [Get into the automated test with Selenium](https://www.youtube.com/watch?v=iRMuO1t_Luo)

- Library
	- [List of plugins](https://docs.google.com/spreadsheets/d/13Nn4gl1hqwp-bIZpWM820VWXLkuXlTFQSqizTS0_cFE)
	- [Wordpress plugin list to be installed](https://docs.google.com/spreadsheets/d/1Sbg-MHzdhPGGxZ38s8Py1jDKseVrtTL0nThu6SlFfl4) for SEO, Security, and Scale purpose

- Performant
	- [Website Performance Optimization from Google Devs](https://www.udacity.com/course/website-performance-optimization--ud884) [Watch] it's a free course, once signup you may skip to the "Critical Rendering Path" section.
	- [Optimising the front end for the browser](https://hackernoon.com/optimising-the-front-end-for-the-browser-f2f51a29c572#.ox4i5ff7d)
	- [Caching Explained](https://cachingexplained.com/)
	- [How Browsers Work: Behind the scenes of modern web browsers](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/) If you really want to deep dive.
	- [Image optimization](https://images.guide/)

- Analytics skill (GA)
	- [The Google Analytics Setup I Use on Every Site I Build](https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/)

- SEO
	- [The Definitive Guide To SEO In 2018](https://backlinko.com/seo-this-year)
	- [What should be in HEAD](https://gethead.info/)
	- [SEO Short Video Course / Duration 1:49:47](https://coursehunters.net/course/Znakomstvo-s-SEO)
	- [Modern SEO Video Course / Duration 04:13:10](https://coursehunters.net/course/frontendmaster-modern-seo)
	- [Vue - Creating beautiful websites with SEO support Video Course / Duration 01:48:11](https://coursehunters.net/course/vue-sozdaem-krasivye-sayty-s-podderzhkoy-seo)

- Tools
- Redux
	- [Free online course from the author of redux](https://egghead.io/courses/getting-started-with-redux) [Watch]
	- [The Inner Workings Of Virtual DOM](https://medium.com/@rajaraodv/the-inner-workings-of-virtual-dom-666ee7ad47cf)

### JS Expert
- So [you choose to be JS expert](js-expert.md)
### Front-End & Designer (FEDX)
- [Bridging The Gap Between Design & Development](https://blog.prototypr.io/8-tips-on-bridging-the-gap-between-design-development-88c4cf24a0a6)
- This is [document](https://docs.google.com/document/d/1ugyDrMXdNgfCDHdasCEIaYWhulAZRryaQkNRA48Vu-M/edit#) that was created by FE and Designer
- Also this should be a **checklist** for designers before hands over to the FE team. [The Front-End Design Checklist](https://frontenddesignchecklist.io/)

### Browsers, devices, and email supported
- [Document](https://docs.google.com/document/d/15i2v5tUx5pZ0tjlDwxqlrBTD77tgTqRinX80M4r7Gh0) that we update every quarter to make sure we support the right market for our client

### Checklist before going to production
- [Product launch checklist](https://docs.google.com/document/d/1a9cZI6RM2AdChoN9u0uXgeKNpo2mlx8cRh4recW-xys/edit)
- Performance checklist
	- Test against 3G network and **Disabled cached**
	- "[Time to First Meaningful Paint](https://www.youtube.com/watch?v=wFwogd4CdwY)" should be under 3 seconds
	- Measure Performance with the [RAIL Model](https://developers.google.com/web/fundamentals/performance/rail)
- Analytics checklist (GA, Sentry, Hotjar, etc.)
- [Code Review checklist](https://ana-balica.github.io/2017/02/21/code-review-checklist/)